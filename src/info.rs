use goblin::Object;
use std::path::Path;
use std::fs::File;
use std::io::Read;
use std::error::Error;
use std::ffi::OsString;
use std::ffi::OsStr;
use std::fmt;

pub trait ImportLibraryInfo {
    fn get_import_libraries(&self) -> &[String];
}

pub trait BinaryInfo {
    fn get_binary_name(&self) -> &OsStr;
    fn get_binary_type(&self) -> BinaryType;
}

#[derive(Clone, Copy, Debug)]
pub enum BinaryType {
    Elf,
    Elf64,
    Pe,
    Pe64,
}

impl fmt::Display for BinaryType {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match *self {
            BinaryType::Elf => write!(f, "ELF"),
            BinaryType::Elf64 => write!(f, "ELF64"),
            BinaryType::Pe => write!(f, "PE"),
            BinaryType::Pe64 => write!(f, "PE64"),
        }
    }
}

pub trait ExportFunctionInfo {
    fn get_export_functions(&self) -> &[&str];
}

pub trait ImportFunctionInfo {
    fn get_import_functions(&self) -> &[&str];
    fn get_import_function_by_library_name(&self, name: &str) -> &[&str];
}

//
// helpers
//

fn generic_parse<'a>(f: &mut File, buffer: &'a mut Vec<u8>) -> Result<Object<'a>, String> {
    // It's ok not to be able to probe file size
    // for now
    if let Ok(meta) = f.metadata() {
        let buffer_len = buffer.len();
        buffer.reserve(meta.len() as usize - buffer_len);
    }

    if f.read_to_end(buffer).is_ok() {
        Object::parse(buffer).map_err(|e| e.description().to_owned())
    } else {
        Err("Cannot read data from incoming file".to_string())
    }
}

#[derive(Debug)]
pub struct Binary {
    name: OsString,
    bin_type: BinaryType,
    required_libs: Vec<String>,
}

impl Binary {
    pub fn from_file(path: &Path) -> Result<Self, String> {
        let mut buffer: Vec<u8> = vec![];
        let file_r = File::open(path);
        file_r
            .map_err(|e| e.description().to_owned())
            .and_then(|mut file| generic_parse(&mut file, &mut buffer))
            .and_then(|obj| match obj {
                Object::PE(p) => {
                    Ok(Binary {
                        name: path.file_name().unwrap().to_owned(),
                        bin_type: if p.is_64 {
                            BinaryType::Pe64
                        } else {
                            BinaryType::Pe
                        },
                        required_libs: p.libraries.iter().cloned().map(|s| s.to_owned()).collect(),
                    })
                }
                Object::Elf(e) => {
                    Ok(Binary {
                        name: path.file_name().unwrap().to_owned(),
                        bin_type: if e.is_64 {
                            BinaryType::Elf64
                        } else {
                            BinaryType::Elf
                        },
                        required_libs: e.libraries.iter().cloned().map(|s| s.to_owned()).collect(),
                    })
                }
                _ => Err("Not a supported binary file".to_owned()),
            })
    }
}

impl BinaryInfo for Binary {
    fn get_binary_name(&self) -> &OsStr {
        &self.name
    }

    fn get_binary_type(&self) -> BinaryType {
        self.bin_type
    }
}

impl ImportLibraryInfo for Binary {
    fn get_import_libraries(&self) -> &[String] {
        &self.required_libs
    }
}
