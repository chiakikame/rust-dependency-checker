use clap;
use clap::App;
use clap::Arg;
use clap::ArgMatches;
use std::ffi::OsString;
use std::path::Path;
use std::path::PathBuf;

pub struct AppOptions {
    path_set: Vec<PathBuf>,
    input_binary: PathBuf,
    input_bin_only: bool,
}

impl AppOptions {
    pub fn get_path_set(&self) -> &[PathBuf] {
        &self.path_set
    }

    pub fn get_input_binary_path(&self) -> &Path {
        &self.input_binary
    }

    pub fn get_process_input_bin_only(&self) -> bool {
        self.input_bin_only
    }
}

pub fn parse_options_from<I, T>(arg_os_iter: I) -> Result<AppOptions, clap::Error>
where
    I: Iterator<Item = T>,
    T: Into<OsString> + Clone,
{
    new_app().get_matches_from_safe(arg_os_iter).map(|m| from_match(&m))
}

pub fn parse_options() -> AppOptions {
    from_match(&new_app().get_matches())
}

fn new_app() -> App<'static, 'static> {
    let path_arg = Arg::with_name("path")
        .short("p")
        .takes_value(true)
        .multiple(true)
        .number_of_values(1)
        .validator_os(|v| {
            let p = PathBuf::from(v);
            if p.exists() && p.is_dir() {
                Ok(())
            } else {
                let mut msg = OsString::from("Path ");
                msg.push(v);
                msg.push(" is not found, or it's not a directory");
                Err(msg)
            }
        })
        .help(
            "Path set, contains paths in which contains dependency binaries",
        );

    let input_bin_arg = Arg::with_name("input")
        .short("i")
        .long("input")
        .validator_os(|v| {
            let p = PathBuf::from(v);
            if p.exists() && p.is_file() {
                Ok(())
            } else {
                let mut msg = OsString::from("Path ");
                msg.push(v);
                msg.push(" is not found, or not a file");
                Err(msg)
            }
        })
        .takes_value(true)
        .number_of_values(1)
        .help("Input binary file for dependency resolution")
        .required(true);

    let current_bin_only_arg = Arg::with_name("self-only").long("self-only");

    App::new("dependency-checker")
        .arg(path_arg)
        .arg(input_bin_arg)
        .arg(current_bin_only_arg)
}

fn from_match(matches: &ArgMatches) -> AppOptions {
    let input_bin_path = PathBuf::from(matches.value_of_os("input").unwrap());
    let path_set: Vec<PathBuf> = if let Some(values) = matches.values_of_os("path") {
        values.map(PathBuf::from).collect()
    } else {
        vec![]
    };
    let input_bin_only = matches.is_present("self-only");

    AppOptions {
        input_binary: input_bin_path,
        path_set,
        input_bin_only,
    }
}
