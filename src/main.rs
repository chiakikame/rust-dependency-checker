extern crate clap;
extern crate goblin;

mod args;
mod info;

use args::parse_options;
use info::Binary;
use info::BinaryInfo;
use info::ImportLibraryInfo;

fn main() {
    let options = parse_options();
    let input_bin_name_string = options.get_input_binary_path().to_string_lossy();

    if options.get_process_input_bin_only() {
        println!("List binary dependency of {}", input_bin_name_string);

        let bin_r = Binary::from_file(options.get_input_binary_path());

        if let Ok(bin) = bin_r {
            println!("Binary name: {}", input_bin_name_string);
            println!("Binary type: {}", bin.get_binary_type());
            println!("Required libraries");
            for lib in bin.get_import_libraries() {
                println!("\t{}", lib);
            }
        } else {
            println!("Error when processing {}", input_bin_name_string);
            println!("{}", bin_r.unwrap_err().to_owned());
        }
    } else {
        println!("Not yet implemented");
    }
}
